#!/bin/bash

## Usage:
## ./cellranger_mkfastq.sh /path/illumina-data/ /path/lanes.csv /path/fastq

## Setup -----------------------------------------------------------------------
source ~/.bashrc

DATA_DIR=$(readlink -f $1)  ## directory where data is held
CSV_PATH=$(readlink -f $2)  ## path to simple CSV with lane, sample, and index cols
                            ##   see cellranger mkfastq --help for --csv
OUT_PATH=$(readlink -f $3)  ## directory to place fastq files


## Exec ------------------------------------------------------------------------
CLUSTER_CMD="source ~/.bashrc; cellranger mkfastq --run=$DATA_DIR --simple-csv=$CSV_PATH --output-dir=$OUT_PATH --delete-undetermined"

sbatch \
    -D $OUT_PATH \
    -e /fh/fast/gottardo_r/ramezqui_working/slurm/master-%j.err \
    -o /fh/fast/gottardo_r/ramezqui_working/slurm/master-%j.out \
    -J 10x_mkfastq \
    -n 1 -c 16 --mem=256000 -p largenode \
    --mail-user=robert.amezquita@fredhutch.org \
    --mail-type=ALL \
    --wrap="$CLUSTER_CMD"
    

