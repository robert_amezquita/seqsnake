


import os
import fnmatch

## 00. final output  -------------------------------------------------------------
rule all:
    input:
        expand("shifted/{sample}.bam", sample=config["samples"])

## 01. cutadapt ------------------------------------------------------------------
indir = 'fastq'
samples = os.listdir(indir)
samples = sorted(list(set(samples)))  ## uniqify sample list

fi = [os.listdir(f) for f in [indir + '/' + m for m in samples]]
fi = [item for sublist in fi for item in sublist] ## flatten lists
fq = fnmatch.filter(fi, '*.fastq*') ## filter down to only fastq data
fq.sort() ## reorder so that files are sorted as R1_001, R1_002, R2_001, R2_002, etc.

def find_files(wildcards):
    matches = fnmatch.filter(fq, wildcards+'*')
    files = [indir + '/' + wildcards + '/' + m for m in matches]
    return(files)

rule cutadapt:
    input:
        lambda wildcards: find_files('{sample}'.format(sample=wildcards.sample))
    output:
        R1='cutadapt/{sample}/{sample}_R1.fastq.gz',
        R2='cutadapt/{sample}/{sample}_R2.fastq.gz'
    threads: 8
    params:
        adapters='/fh/fast/gottardo_r/ramezqui_working/reference/adapters/adapters.fasta'
    log: "logs/cutadapt/{sample}.log"
    shell:
        "mkdir -p cutadapt/{wildcards.sample}; "
        ""
        "cutadapt "
        "--cores={threads} "
        "-a file:{params.adapters} "   # Adapter sequences 3'
        "-A file:{params.adapters} "   # Adapter sequence 5'
        "--error-rate=0.2 "            # allowed adapter error rate
        "-m 15 --trim-n --max-n=0.2 "  # Min Length, trim N's, and Max N
        "--quality-cutoff=20,20 "      # quality cutoff
        "--times=5 "                   # check if adapter inserted <5x
        "--output={output.R1} "  
        "--paired-output={output.R2} "
        "{input} "
        "> {log}"


## 02. bowtie2 alignment  ------------------------------------------------------
indir = 'cutadapt'
samples = os.listdir(indir)
samples = sorted(list(set(samples)))  ## uniqify sample list

rule bowtie2_pe:
    input:
        R1=indir + '/{sample}/{sample}_R1.fastq.gz',
        R2=indir + '/{sample}/{sample}_R2.fastq.gz'
    output:
        SAM='bowtie2/{sample}/mapped.sam'
##      UNMAPPED='bowtie2/{sample}/unmapped_R%.fastq.gz'
    threads: 16
    log: "logs/bowtie2/{sample}.log"
    params:
        bt2_index='/fh/fast/gottardo_r/ramezqui_working/reference/Ensembl/bt2-index/GRCh38/index',
	min_insert=10,
	max_insert=700
    shell:
        "mkdir -p bowtie2/{wildcards.sample}; "
        ""
        "bowtie2 "
	"--local --very-sensitive-local "
	"--no-mixed --no-discordant --no-unal "
	"--minins {params.min_insert} --maxins {params.max_insert} "
        "--threads {threads} "
        "--un-conc-gz bowtie2/{wildcards.sample}/unmapped_R%.fastq.gz " # {output.UNMAPPED}
	"-x {params.bt2_index} "
        "-1 {input.R1} -2 {input.R2} "
        "-S {output.SAM} 2> {log}"

        


## -----------------------------------------------------------------------------

rule all:
    input:
        expand("cutadapt/{sample}/{sample}_{pair}.fastq.gz",
               sample=samples, pair=["R1","R2"])

rule all:
    input:
        expand("bowtie2/{sample}/mapped.sam", ##{sample}_{type}", type=[''])
               sample=samples)

