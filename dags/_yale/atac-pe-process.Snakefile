rule all:
    input:
        expand("shifted/{sample}.bam", sample=config["samples"])

rule cutadapt:
    input:
        fastq1="fastq/{sample}_1.fastq.gz",
        fastq2="fastq/{sample}_2.fastq.gz"
    output:
        fastq1="trimmed/{sample}_1.fastq.gz",
        fastq2="trimmed/{sample}_2.fastq.gz",
        ##qc="trimmed/{sample}.qc.txt"
    params:
        "-a AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC ",
        "-A AGATCGGAAGAGCGTCGTGTAGGGAAAGAGTGTAGATCTCGGTGGTCGCCGTATCATT ",
        "-q 20"
    log:
        "logs/cutadapt/{sample}.log"
    shell:
        "cutadapt"
        " {params}"
        " -o {output.fastq1}"
        " -p {output.fastq2}"
        " {input.fastq1} {input.fastq2}"
        " > {log}"

rule bowtie2:
    input:
        fastq1="trimmed/{sample}_1.fastq.gz",
        fastq2="trimmed/{sample}_2.fastq.gz"        
    output:
        "mapped/{sample}.bam"
    params:
        ##index="index/genome", ## for testing        
        index="/home/bioinfo/genomes/igenomes/Mus_musculus/UCSC/mm10/Sequence/Bowtie2Index/genome",
        extra="--very-sensitive --maxins 2000 --no-mixed --no-discordant"
    threads:
        8
    resources:
        memperthread=750
    log:
        "logs/bowtie2/{sample}.log"
    shell:
        "(bowtie2 --threads {threads} {params.extra} "
        "-x {params.index} "
        "-1 {input.fastq1} -2 {input.fastq2} | "
        "samtools view -Sbh --threads {threads} | "
        "samtools sort --threads {threads} -m {resources.memperthread}M -o {output}) 2> {log}"

rule shift:
    input:
        "mapped/{sample}.bam"
    output:
        "shifted/{sample}.bam"
    log:
        "logs/shift/{sample}.log"
    resources:
        memperthread=750
    threads:
        8
    shell:
        "samtools view -h {input} | "
        "awk '{{ FS = \"\t\"; OFS = \"\t\" }} {{ "
        "  if (substr($1,1,1) == \"@\")   {{ print $0 }} else "
        "  if ($3 == \"MT\" || $3 == \"M\") {{          }} else "
        "  if ($2 == 0 || $2 == 99 || $2 == 147)  {{ $4 = $4 + 4; print $0 }} else "
        "  if ($2 == 16 || $2 == 83 || $2 == 163) {{ $4 = $4 - 5; print $0 }} "
        "  else {{ print $0 }} "
        "}}' | samtools view -b - > shifted/{wildcards.sample}.shift.bam; "
        ""
        "samtools sort --threads {threads} -m {resources.memperthread}M "
        "-O bam shifted/{wildcards.sample}.shift.bam > {output}; "
        ""
        "samtools index {output}; "
        ""
        "rm -f shifted/{wildcards.sample}.shift.bam"
        
rule filter_pe:
    input:
        "shifted/{sample}.bam"
    output:
        filtered="filtered/{sample}.bam"
    params:
        picard="ASSUME_SORTED=true REMOVE_DUPLICATES=true"
    log:
        "logs/picard/{sample}.markdup.metrics.log"
    resources:
        mem=14
    shell:
        "picard -Xmx{resources.mem}g MarkDuplicates "
        "I={input} O=filtered/{wildcards.sample}.md.bam "
        "{params.picard} "
        "METRICS_FILE={log} "
        "VALIDATION_STRINGENCY=LENIENT; "
        ""
        "export CHROMOSOMES=$(samtools view -H filtered/{wildcards.sample}.md.bam | "
        "grep '^@SQ' | cut -f 2 | grep -v -e _ -e chrM -e chrY -e 'VN:' | "
        "sed 's/SN://' | xargs echo); "
        ""
        "samtools index filtered/{wildcards.sample}.md.bam; "
        ""
        "samtools view -b -h -f 3 -F 4 -F 8 -F 256 -F 1024 -F 2048 -q 30 "
        "filtered/{wildcards.sample}.md.bam $CHROMOSOMES > {output.filtered}; "
        ""
        "samtools index {output.filtered}; "
        ""
        "rm filtered/{wildcards.sample}.md.bam*"

        ## Removed this from original `grep -v ` snippet:
        ## -e chrX

