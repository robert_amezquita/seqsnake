import snakemake.utils

## Process treatment/control sample pairings -------
X = config["samples"]

newkey = []
for key in X:
    newkey.append(key.split('____'))

TREATMENT = [item[0] for item in newkey]    
CONTROL = [item[1] for item in newkey]

## ---------------------------------------

rule all:
    input:
        expand("filtered/{sample}.bam", sample=TREATMENT)

rule cutadapt:
    input:
        "fastq/{sample}_1.fastq.gz",
    output:
        fastq="trimmed/{sample}_1.fastq.gz",
        ##qc="trimmed/{sample}.qc.txt"
    params:
        "-a AGATCGGAAGAGCACACGTCTGAACTCCAGTCAC -q 20"
    log:
        "logs/cutadapt/{sample}.log"
    shell:
        "cutadapt"
        " {params}"
        " -o {output.fastq}"
        " {input}"
        " > {log}"

rule bowtie2:
    input:
        fastq="trimmed/{sample}_1.fastq.gz"
    output:
        "mapped/{sample}.bam"
    params:
        index="/home/bioinfo/genomes/igenomes/Mus_musculus/UCSC/mm10/Sequence/Bowtie2Index/genome",
        ##index="index/genome",
        extra="--very-sensitive"
    threads:
        8
    resources:
        memperthread=750
    log:
        "logs/bowtie2/{sample}.log"
    shell:
        "(bowtie2 --threads {threads} {params.extra} "
        "-x {params.index} "
        "-U {input.fastq} | "
        "samtools view -Sbh --threads {threads} | "
        "samtools sort --threads {threads} -m {resources.memperthread}M -o {output}) 2> {log}"
        
rule filter_se:
    input:
        "mapped/{sample}.bam"
    output:
        ##metrics="filtered/{sample}.picard.markdup.metrics",
        filtered="filtered/{sample}.bam"
    params:
        picard="ASSUME_SORTED=true REMOVE_DUPLICATES=true"
    log:
        "logs/picard/{sample}.markdup.metrics.log"
    resources:
        mem=14
    shell:
        "picard -Xmx{resources.mem}g MarkDuplicates "
        "I={input} O=filtered/{wildcards.sample}.md.bam "
        "{params.picard} "
        "METRICS_FILE={log} "
        "VALIDATION_STRINGENCY=LENIENT; "
        ""
        "export CHROMOSOMES=$(samtools view -H filtered/{wildcards.sample}.md.bam | "
        "grep '^@SQ' | cut -f 2 | grep -v -e _ -e chrM -e chrY -e 'VN:' | "
        "sed 's/SN://' | xargs echo); "
        ""
        "samtools index filtered/{wildcards.sample}.md.bam; "
        ""
        "samtools view -b -h -F 4 -F 8 -F 256 -F 1024 -F 2048 -q 30 "
        "filtered/{wildcards.sample}.md.bam $CHROMOSOMES > {output.filtered}; "
        ""
        "samtools index {output.filtered}; "
        ""
        "rm filtered/{wildcards.sample}.md.bam*"

        ## Removed this from original `grep -v ` snippet:
        ## -e chrX
